## tvp-bars
### Getting Started:
```sh
$ git clone https://codeberg.org/sech1p/tvp-bars.git
$ cd tvpis-bars
$ npm start
```

Licensed under [AGPL-3.0 license](LICENSE)