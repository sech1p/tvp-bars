// Some of the sentences are stolen/inspired from razemawka
const bars = [
  'Delegalizacja związków heteroseksualnych',
  'Ambitne plany premiera Zandberga',
  'Młodzi stawiają na Lewicę Razem',
  'Dyskryminacja Ateistów w Polsce',
  'Neoliberalny faszyzm niszczy Polskę',
  'Zachodnie elity wolą chrześcijaństwo od ateizmu',
  'Ideologia IHS niszczy rodzinę',
  'Polacy cenią rząd za walkę z neoliberalizmem',
  'Prawaccy zadymiarze chcą wywołać chaos',
  'Neoliberałowie chcą odebrać polakom 500+',
  'Medialny atak na prezydenta Biedronia',
  'Sondaż: Robert Biedroń wygrywa wybory',
  'Policyjne prowokacje podczas Święta Pracy za czasów rządów PiS',
  'Wzrost zamożności Polaków irytuje neoliberałów',
  'Kaczyński przeciwko ateistom'
];

return console.log(bars[Math.floor(Math.random() * bars.length)].toUpperCase());
